PREREQUISITE/ INSTALLATION:

To clone this repository, you'll obviously need Git.
The above assumes you're using Windows...
You must have Python ver. 2.7 installed (not always).
You must have Node.js ver. 10.15.3 installed.
You must also have the Java Development Kit installed in order to run the Selenium-Standalone server.
This test will run Chrome, so make sure you download the latest version of the Chrome browser.


CLONING THE REPOSITORY:

Open 'Command Prompt' in Windows and navigate to where you want to download the repository.
Clone the repository and it'll be placed inside a folder called 'testing-booking.com':

git clone https://Yuri-Ananyev@bitbucket.org/Yuri-Ananyev/testing-booking.com.git


INSTALL DEPENDENCIES:

In 'testing-booking.com' folder run the command:
npm install

npm install --global --production windows-build-tools   - add Python 2.7
npm install -g node-gyp

If you do not need to install the "python" on this computer, you can copy the contents of the "fibers" archive to the directory ". \ Node_modules" (to get .. \ testing-booking.com \ node_modules \ fibers \) and then restart:  npm install

To run all test files inside 'test' folder, run the following command:
npm test


-----
**Selenium WebdriverIO Boilerplate with Mocha + Chai**

node ( Version 10 ) <https://nodejs.org>

selenium-standalone <https://github.com/vvo/selenium-standalone>

webdriverio <https://github.com/webdriverio/webdriverio/>

Mocha <https://mochajs.org/>

Chai <https://chaijs.com> ( test are written in Chai with es6 )