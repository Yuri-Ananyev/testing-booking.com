const NumChild=5; // N number of children between [1;10]

//determine the value of day on 10 days from today
function futureDay () {
	let nowDay = new Date();
	return Math.round(nowDay.getTime()/86400000+10)*86400000;
};

class PageObj {

	URL() {
		const urlLocator = 'https://www.booking.com/';
		browser.url(urlLocator);
	};

	get ToStrangers(){
		return $('//label[@id="xp__guests__toggle"]');
	};

	get child(){
		return $('(//button[@class="bui-button bui-button--secondary bui-stepper__add-button"])[2]');
	};

	get childAges(){
		return $$('//select[@name="age"]');
	};

	choiceChildNum() {
		for (let i=0; i < NumChild; i++) {
		this.child.click();
		}
	};

	AgeChild() {
		//number of elements found is equal to the number of children
		NumChild == this.childAges.length;
		//we will check every element of entering the age of children
		for (let i=0; i < this.childAges.length; i++) {
			this.childAges[i].$$('option')[i+2].click();
		}
	};


	get anyCity(){
		return $('//div[@class="unified-postcard__content   unified-postcard__content_pe"]');
	};

	get waitToHotel(){
		return $('//div[@id="hotellist_inner"]');
	};

	get waitToCal(){
		return $('//div[@class="c2-calendar"]');
	};

	get waitToCalClose(){
		return $('//button[@class="c2-calendar-close-button c2-calendar-close-button-clearappearance"]');
	};

	waitToNoResult(){
		if (this.priceList.length > 0) {
			this.priceList.$$('non-existent locator').waitForVisible();
		};				
	};

	get ToNoResult(){
		return $('//a[@class="b-button b-button_primary sr_cta_button no_dates_click jq_tooltip"]');				
	};

	clickToCal() {
		const dataLocator = '//td[@data-id="' + futureDay() + '"]';
		browser.click(dataLocator);
		browser.pause(2000);
	};

	get clickToSubmit(){
		return $('//button[@class="sb-searchbox__button  "]');
	};

	get allList(){
		return $$('//div[@class="sr_item  sr_item_new sr_item_default sr_property_block  sr_flex_layout"]');
	};

	get priceList(){
		return $$('//div[@class="bui-price-display__value prco-inline-block-maker-helper"]');
	};

	get missList(){
		return $$('//div[@class="fe_banner__message"]');
	};

	checkResult() {
		//the number of items found in the entire list is equal to the sum of items with a price and no price
		this.allList.length == (this.priceList.length + this.missList.length);
		for (let i=0; i < this.allList.length; i++) {
			(this.allList.$$(this.priceList)[i].waitForVisible() || this.allList.$$(this.missList)[i].waitForVisible());
		}
	};

};

module.exports = new PageObj();


