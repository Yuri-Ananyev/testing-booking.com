var PageObj = require('./po/common2.js');

describe('User is required to specify booking date to see booking price', ()=> {

	it('1.choose any city from the menu below', ()=> {
		PageObj.URL();
		PageObj.anyCity.click();
		PageObj.waitToHotel.waitForVisible();		//1.1.page with listed hotels is opened
		PageObj.waitToCal.waitForVisible();			//1.2.calendar for specifying check in date is opened
		//PageObj.waitToCalClose.click();			//If you want to close the calendar
		PageObj.waitToNoResult();					//1.3.no result entry containing booking price or booking status
	});

	it('2.Click "show prices" button for any hotel / calendar for specifying check in date is opened', ()=> {
		PageObj.ToNoResult.click();
		PageObj.waitToCal.waitForVisible();
	});
	
	it('3.Set any dates for check in and out', ()=> {
		PageObj.clickToCal();
	});

	it('4.Submit search form', ()=> {
		PageObj.clickToSubmit.waitForEnabled();
		PageObj.clickToSubmit.click();
		PageObj.checkResult();		//Ex.: each result entry has booking price or banner saying no free places
	});

});

