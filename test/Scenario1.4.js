var PageObj = require('./po/common2.js');

describe('User is able to specify age of each child', ()=> {

	it('1.open menu for selecting strangers number', ()=> {
	        PageObj.URL();
		PageObj.ToStrangers.click();
		PageObj.child.waitForEnabled();
	});
	
	it('2.specify N number of children (N > 1)', ()=> {
		PageObj.choiceChildNum();
		PageObj.AgeChild();		//Ex.:the number of age inputs is equal to N
	});
	
});